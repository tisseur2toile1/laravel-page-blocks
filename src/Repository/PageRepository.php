<?php

namespace Pilyavskiy\PB\Repository;

use Pilyavskiy\PB\Model\Page;

class PageRepository
{
    public function read(?string $slug = ''): ?Page
    {
        if (empty($slug)) {
            $slug = '/';
        }

        $page = Page::getPageByRoute($slug);

        return $page;
    }
}
