<?php

namespace Pilyavskiy\PB;

use Pilyavskiy\PB\Console\PBModelMakeCommand;
use Illuminate\Support\ServiceProvider;
use Pilyavskiy\PB\Repository\PageRepository;

class PBServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PageRepository::class, function () {
            return new PageRepository();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/../../resources/views'), 'laravel-page-blocks');
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../migrations'));
        $this->loadRoutesFrom(realpath(__DIR__.'/../../routes/pb.php'));

        $this->mergeConfigFrom(
            realpath(__DIR__.'/../../config/laravel-page-blocks.php'), 'laravel-page-blocks'
        );

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/laravel-page-blocks'),
            __DIR__.'/../../migrations' => database_path('migrations'),
            __DIR__.'/../../config' => config_path(),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                PBModelMakeCommand::class,
            ]);
        }
    }

    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(__DIR__.'/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}

